# Finding 10 World Players of the Year and extracting information to .csv file

from bs4 import BeautifulSoup
import requests
import lxml
import pandas as pd

base_url = "https://en.wikipedia.org/wiki/World_Soccer_(magazine)"

# Send Get http request
page = requests.get(base_url)

# Werify we had a successful Get request webpage call 
if page.status_code == requests.codes.ok:
  # Get the whole webpage in beautiful Soup format
  bs = BeautifulSoup(page.text, 'lxml')

# Find somthing you spcify in  the httml 
list_all_players = bs.find('table', class_='multicol').find('ul').find_all('li')
# Find last 10 players of the list
last_ten_players = list_all_players[-10::]

# Data of players
data = {
  "Year" : [],
  "Country" : [],
  "Player" : [],
  "Team" : []
}


for list_item in last_ten_players:
  
  # Get the year of the player and save it in the data dictionary 
  year = list_item.find('span').previousSibling.split()[0]
  if year:
    data['Year'].append(year)
  else:
    data['Year'].append('None')

  # Get the country of the player and save it in the data dictionary
  country = list_item.find('a')['title']
  if country:
    data['Country'].append(country) 
  else:
    data['Country'].append('None')

  # Get the name of the player and save it in the data dictionary
  player = list_item.find_all('a')[1].text
  if player:
    data['Player'].append(player)
  else:
    data['Player'].append('None')

  #  Get the team of the player and save it in the data dictionary
  team = list_item.find_all('a')[2].text # or ['title']
  if player:
    data['Team'].append(team)
  else:
    data['Team'].append('None')


# Making data in a table form
table = pd.DataFrame(data)

# Making raws start with " 1 " 
table.index = table.index + 1

# Save the table to csv file
table.to_csv('Top_10_players',sep = ',',index = False, encoding = 'UTF-8')












